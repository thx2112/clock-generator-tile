/**********************************************************************************************************

					Clock Generator

					1/13/2017

		Generates a Sync24 or 4 PPQN Clock

Features:

-Start/Stop input.
-CV control of tempo.
-Tap tempo.
-Reset out on every 96 clocks.
-Configurable for Sync24 or 4 PPQN clocks.
-<1 microsecond clock rise jitter (with this version of code).
-10 to 260 BPM

	TO DO:

	-Fractional BPM
	-Better display of state changes

**********************************************************************************************************/

#define F_CPU 16000000UL
#include <avr/io.h>
#include <EEPROM.h>

#define clockOutputPin 3		//	PB3
#define startInputPin 1			//	PB1
#define resetIOPin 0			//	PB0
#define tempoPin A1
#define LEDPin 4				//	PB4

int tempo;
int oldtempo;
long period;
long endPulseTime;
long pulseDuration = 5000;		//	Length of pulse in microseconds (/1000 = milliseconds)
long now;
long endPeriod;
int count = 0;
long endLEDTime;
int oldTempo;
int oldBPM;
int pBPM;
int npBPM;
boolean changed;
boolean resetStarted;
int PPQN = 24;
int BPM;
int lcounter = 0;

boolean start;
boolean started = false;
boolean running = false;
boolean ended = false;
boolean disableLED = false;
boolean tapMode = false;

int tapButton = resetIOPin;
int tapCounter = 0;
int previousMillis;
int currentMillis;
int tapInterval;
int tap[4];
int finalTapInterval = 1000;
int hTimer;
boolean timerDone;
boolean tapChanged;

int buttonState;
int lastButtonState = LOW;
long lastDebounceTime = 0;
long debounceDelay = 5;

void setup()
{
	MCUCR |= (1 << PUD);		// Disable pull-ups so we can change pin mode on the fly.

	randomSeed(analogRead(A1));

	pinMode(startInputPin, INPUT);
	pinMode(resetIOPin, INPUT);	//	Set as input for checkSetup();

	pinMode(LEDPin, OUTPUT);
	pinMode(clockOutputPin, OUTPUT);

	checkSetup();
	delay(100);
	tempo = analogRead(tempoPin);
	changed = true;
	setTempo();

	//	Blink LED on startup.

	digitalWrite(LEDPin, HIGH);
	delay(100);
	digitalWrite(LEDPin, LOW);
	delay(100);
}

void loop()
{
	checkHoldTimer();
	setTempo();
	started = checkStartPin();
	while (started) {
		setTempo();
		now = micros();
		if (!running) {										//	Start of a pulse. Positive slope.
			//	Send RESET if start of PPQN24 sequence
			if (count == 0) {
				if (!tapMode) {								//	Disable RESET if in tap mode.
					startReset();
				}
			}
			startPulse();
			running = true;
			if (lcounter == 0) {
				startLED();
			}
			lcounter++;
			if (lcounter == 4) { lcounter = 0; }
			ended = false;
		}

		if (running && now >= endPulseTime) {				//	End of a clock pulse. Negative slope.
			if (!ended)			//	Do this stuff only once at end of the clock pulse.
			{
				endPulse();
				endLED();
				ended = true;

				// Do slow stuff here before the pulse starts again.

				if (count >= 0) {							//	RESET longer than clock.
					if (!tapMode) {							//	Disable RESET if in tap mode.
						endReset();
					}
				}
				count++;
				if (count >= 96) { count = 0; }

				checkHoldTimer();
			}
		}
		started = checkStartPin();
		setTempo();

		if (running && now >= endPeriod) {
			running = false;
		}

		if (!started) {
			//	Clean up after end.
			running = false;
			endPulse();
			count = 0;
			if (!tapMode) {									//	Disable RESET if in tap mode.
				endReset();
			}
			endLED();
			endReset;
		}
	}
}

/***********************************************************************************************************

		Hold Timer

Check if the reset pin is being held high (presumably by a button) for two seconds.

Toggle between tap-tempo mode and regular CV/Knob mode.

*/

void checkHoldTimer() {
	if (!resetStarted) {		//	Wait if there's currently a reset.
		setResetAsInput();
		bool temp = checkResetPin();
		if (temp == HIGH)
		{
			int timer = millis();
			if (hTimer == 0)
			{
				hTimer = timer;
			}

			if ((timer >= (hTimer + 2000)) && timerDone)	//	Hold for two seconds to set modes.
			{
				digitalWrite(LEDPin, HIGH);
				disableLED = true;
				timerDone = false;							//	Only reset timer if button raised.
				bool temprun = tapMode;						//	Toggle between tap modes.
				if (temprun)
				{
					tapMode = false;
				}
				if (!temprun)
				{
					tapMode = true;
					finalTapInterval = 60000 / BPM;			//	Start tapmode with pots BPM
				}
				//hTimer = 0;
			}
		}
		else
		{
			hTimer = 0;
			disableLED = false;
			timerDone = true;	//	Prevent change if button kept held down.
			endLED();
		}
		if (tapMode == true) {
		}
		else {
			setResetAsOutput();
		}
	}
}

/***********************************************************************************************************

			TAP TEMPO

TO DO:

	Fractioned BPM
	-	Do math as float or,
	-	Set BPMx1000 and later reduce by 1000

*/

void tapTempo() {
	boolean reading = checkResetPin();

	if (reading != lastButtonState) {
		lastDebounceTime = millis();
	}

	if ((millis() - lastDebounceTime) > debounceDelay)
	{
		if (reading != buttonState) {
			buttonState = reading;

			if (buttonState == HIGH)
			{
				tapCounter++;
				if (tapCounter == 1)						// First tap
				{
					previousMillis = millis();
				}
				currentMillis = millis();
				tapInterval = currentMillis - previousMillis;
				tap[tapCounter] = tapInterval;
				if (tapInterval > 3000)
				{
					tapCounter = 0;							//	Timeout
				}

				/*if (tapCounter == 0) {}
				if (tapCounter == 1) { analogWrite(LEDPin, 10); }
				if (tapCounter == 2) { analogWrite(LEDPin, 100); }
				if (tapCounter == 3) { analogWrite(LEDPin, 255); }
				if (tapCounter >= 4) { analogWrite(LEDPin, 0); }*/

				if (tapCounter >= 4)
				{
					finalTapInterval = (tap[2] + tap[3] + tap[4]) / 3; // discard first reading and take an average
					tapChanged = true;
					tapCounter = 0;
				}
				previousMillis = currentMillis;
			}
		}
	}
	lastButtonState = reading;
}

void setTempo() {
	if (tapMode)
	{
		tapTempo();

		//if (tapChanged) {
		tempo = analogRead(tempoPin);						//	Use pot to change the tapped BPM.
		npBPM = (tempo / 4) + 10;
		BPM = (60000 / finalTapInterval) + (npBPM - pBPM);
		tapChanged = false;
		changed = true;
		//}
	}
	else
	{
		tempo = analogRead(tempoPin);
		if (tempo != oldTempo) {
			BPM = (tempo / 4) + 10;
			oldTempo = tempo;
			pBPM = BPM;
			changed = true;
		}
	}
	if (changed) {
		if (BPM <= 8) { BPM = 8; }
		if (BPM >= 300) { BPM = 300; }
		period = (60000000 / (BPM*PPQN));
		changed = false;
	}
}

/***********************************************************************************************************

				FAST IO

Note: Pullups globally disabled in setup().

*/

void startPulse() {
	PORTB |= (1 << clockOutputPin);							// clockOutputPin high
	endPulseTime = micros() + pulseDuration;
	endPeriod = micros() + period;
}

void endPulse() {
	PORTB &= ~(1 << clockOutputPin);						// clockOutputPin low
}

void startLED() {
	if (!disableLED) {
		PORTB |= (1 << LEDPin);								// LEDPin high
	}
}

void endLED() {
	if (!disableLED) {
		PORTB &= ~(1 << LEDPin);							// LEDPin low
	}
}

void startReset() {
	PORTB |= (1 << resetIOPin);
	resetStarted = true;
}

void endReset() {
	PORTB &= ~(1 << resetIOPin);
	resetStarted = false;
}

void setResetAsOutput()
{
	DDRB |= (1 << resetIOPin);								// resetIOPin high
}

void setResetAsInput()
{
	//	Might need to check if HIGH then drop to LOW to prevent activating pull-up (then set it back.)
	boolean resetted = false;
	if ((bitRead(PINB, resetIOPin))) {	//&& !tapMode
		PORTB &= ~(1 << resetIOPin);						//	Set resetIOPin low
		resetted = true;
	}

	DDRB &= ~(1 << resetIOPin);								// Set resetIOPin as input

	if (resetted) {
		PORTB |= (1 << resetIOPin);							//	Set PB0 high if was high before
	}
}

boolean checkResetPin()
{
	return (bitRead(PINB, resetIOPin));
}

boolean checkStartPin()
{
	return (bitRead(PINB, startInputPin));
}

/**********************************************************************************************************

				Boot-time Configuration:

-Turn off module
-Patch from input to output (first jack to third jack) to set PPQN.
-Patch 5V to middle jack to set Tap Tempo mode. Leave empty for RESET mode.
	-The two back jumpers need to be set on SEP. POT.
-Turn pot to desired programming position (left, center, right).
	-Left: 4 PPQN
	-Center: nothing
	-Right:	24 PPQN
-Turn power on.
-Module LED will flash for 1 second to show it's been programmed.
-Remove patch cable.
-Module should function as desired, but may need to be power-cycled again (without patch cable).

**********************************************************************************************************/

void checkSetup()
{
	bool fail = false;										//	Optimism is everything

	//	If EEPROM is empty (ie. new firmware uploaded).
	if (EEPROM.read(0) == 255) {
		EEPROM.write(0, 24);
		EEPROM.write(2, 0);
		delay(100);
		digitalWrite(LEDPin, HIGH);
		delay(100);
		digitalWrite(LEDPin, LOW);
		delay(100);
		digitalWrite(LEDPin, HIGH);
		delay(100);
		digitalWrite(LEDPin, LOW);
		delay(100);
	}

	//	Start pulse sequence to check if setup patch cables enabled

	for (int i = 1; i <= 10; i++)
	{
		digitalWrite(clockOutputPin, HIGH);
		delay(random(10) + 1);								//	Not random, but generates a sequence
		if (digitalRead(startInputPin) != HIGH) { fail = true; }
		digitalWrite(clockOutputPin, LOW);
		delay(random(10) + 1);
		if (digitalRead(startInputPin) != LOW) { fail = true; }
	}

	//	If output is connected to input, read the pot and store the value

	if (!fail)
	{
		//	Set base PPQN
		int pot = analogRead(tempoPin);
		if (pot <= 200) {
			EEPROM.put(0, 4);								//	Location 0, 4 PPQN
		}

		if (pot >= 800) {
			EEPROM.put(0, 24);								//	Location 0, 24 PPQN
		}

		//	Set middle jack option, RESET or TAP TEMPO
		tapMode = digitalRead(resetIOPin);
		if (tapMode == HIGH) {
			EEPROM.put(2, 1);								//	Location 2, 1 tap tempo mode
		}

		if (tapMode == LOW) {
			EEPROM.put(2, 0);								//	Location 2, 0 reset out mode
		}

		digitalWrite(LEDPin, HIGH);
		delay(1000);
		digitalWrite(LEDPin, LOW);
		delay(500);
	}

	//	Read the eeprom to get the previously saved setting.

	EEPROM.get(0, PPQN);
	EEPROM.get(2, tapMode);
}